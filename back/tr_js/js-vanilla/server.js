let rezervari = {
  numarRezervariTotale: 3,
  numePersoane: ["Croco", "Tania", "Adrian"], 
  nrPersoaneLaMasa: [6, 2, 3]
};

console.log(rezervari);
rezervari.numePersoane.push("Mircea");
rezervari.nrPersoaneLaMasa.push(10);
rezervari.numarRezervariTotale++;
console.log(rezervari);


let totalGuests = () => {
  let sum = 0;
  let output = [];
  for(let i = 0; i < rezervari.nrPersoaneLaMasa.length; ++i){
    output[i] = rezervari.numePersoane[i] + "-" + rezervari.nrPersoaneLaMasa[i];
    sum += rezervari.nrPersoaneLaMasa[i];
  }
  return output;
};

console.log(totalGuests());